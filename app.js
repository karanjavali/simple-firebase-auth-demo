
(function(){
const config = {
    apiKey: "AIzaSyDBNx4lytmtiiljPl2zda8Qe8uE9BXUCfA",
    authDomain: "fir-9dda7.firebaseapp.com",
    databaseURL: "https://fir-9dda7.firebaseio.com",
    projectId: "fir-9dda7",
    storageBucket: "fir-9dda7.appspot.com",
    messagingSenderId: "628536401731",
    appId: "1:628536401731:web:aad3f188f23a0af4f619f6"
};


firebase.initializeApp(config);
var db = firebase.firestore();  
const email = document.getElementById('email');
const password = document.getElementById('password');
firebase.auth().onAuthStateChanged(firebaseUser=>{
    if(firebaseUser){
        console.log(firebaseUser);
        clear_values();
        display_table();
    }
    else
    {
        document.getElementById('content').style.display='none';
    }
})

}());

function login(){
    const auth = firebase.auth();
    const pass = password.value;
    const user = email.value;
    const promise = auth.signInWithEmailAndPassword(user,pass);
    promise.catch(e=>document.getElementById("error").textContent=e.message);
}

function signUp(){
    const auth = firebase.auth();
    const pass = password.value;
    const user = email.value;
    const promise = auth.createUserWithEmailAndPassword(user,pass);
    promise.catch(e=>document.getElementById("error").textContent=e.message);
}


function logout(){
    const promise = firebase.auth().signOut();
    promise.catch(e=>console.log(e.message));
}
function display_table(){
    firebase.firestore().collection('users').get().then((snapshot) => {
        snapshot.docs.forEach(doc => {
            create_table_row(doc);
        })
    })
}

function clear_values()
{
    document.getElementById('content').style.display='block'; 
    document.getElementById('error').textContent="";  
    document.getElementById('email').value="";
    document.getElementById('password').value="";
}
function create_table_row(doc){
    let tr = document.createElement('tr');
    let td_age = document.createElement('td');
    let td_full_name = document.createElement('td');
    let td_role = document.createElement('td');
    const userList = document.querySelector('#tbl_users') ;
    tr.setAttribute('data-id', doc.id);
    td_age.textContent = doc.data().age;
    td_full_name.textContent = doc.data().full_name;
    td_role.textContent = doc.data().role;

    
    tr.appendChild(td_full_name);
    tr.appendChild(td_age);
    tr.appendChild(td_role);

    userList.appendChild(tr);

}

function addUser(){
    let full_name = document.querySelector("#full_name").value;
    let age = document.querySelector("#age").value;
    let role = document.querySelector("#role").value;
    
  
    if (
      full_name === "" ||
      age === "" ||
      role === "" 
    ) 
    {
      alert("Fields are empty");
    } 
    else 
    {
        firebase.firestore().collection("users").doc().set({
          full_name:full_name,
          age: age,
          role: role,
        })
    }
}